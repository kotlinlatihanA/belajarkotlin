/**
 * Created by SAINTEK 101 on 10/17/2017.
 */
fun main(args: Array<String>) {
    var map= hashMapOf( "nama depan" to "Shofwa", "nama tengah" to "Rahmah")
    map.put("nama belakang", "Rachel")
    println(map.get("nama belakang"))
    println(map["nama belakang"])

    var ar = arrayOf(1,10,22,11)
    println(ar[0])
    var list = mutableListOf(11,22,33,22)
    list[0]=22
    for (item in list) {
        println(item)
    }
}
